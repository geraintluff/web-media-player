"use strict"
var fs = require('fs-extra'), path = require('path'), Promise = require('prms');

Promise.wrap = function (obj, method) {
	return function () {
		var args = [].slice.call(arguments, 0);
		return Promise.callback(function (callback) {
			method.apply(obj, args.concat([callback]));
		});
	};
};
Promise.map = function (entries, mapFn, limit) {
	entries = [].concat(entries);
	limit = limit || Infinity;
	var results = [];
	var indexCounter = 0;
	function next() {
		var index = indexCounter++;
		if (!entries.length) return;
		return Promise.resolve(mapFn(entries.shift(), index)).then(function (r) {
			results[index] = r;
			return next();
		});
	}
	var parallel = [];
	while (parallel.length < limit) {
		parallel.push(next());
	}
	return Promise.all(parallel).then(function () {
		return results;
	});
};

var inputDir = process.argv[2];

var updateLineTimeout, updateLineTimeoutMs = 30, nextUpdateText;
function updateLine(text, sync) {
	nextUpdateText = text;
	function actualUpdate() {
		updateLineTimeout = null;
		var readline = require('readline');
		readline.clearLine(process.stdout);
		readline.cursorTo(process.stdout, 0);
		var maxLength = process.stdout.columns - 1;
		if (nextUpdateText.length > maxLength) {
			nextUpdateText = nextUpdateText.substring(0, maxLength - 3) + '...';
		}
		process.stdout.write('\r' + nextUpdateText);
	}
	if (sync) {
		actualUpdate();
	} else {
		updateLineTimeout = updateLineTimeout || setTimeout(actualUpdate, updateLineTimeoutMs);
	}
}

function magicSort(a, b, splits) {
	splits = splits || [/[^/]+/g, /[^\.]+/g, /[0-9]+|[^\s0-9]+/g];
	var number = /^[0-9]+$/;
	if (splits.length) {
		var split = splits[0];
		var subSplits = splits.slice(1);
		var aParts = a.match(split) || [], bParts = b.match(split) || [];
		while (aParts.length) {
			if (!bParts.length) return 1;
			var cmp = magicSort(aParts.shift(), bParts.shift(), subSplits);
			if (cmp) return cmp;
		}
		if (bParts.length) return -1;
	} else {
		if (number.test(a) && number.test(b)) {
			var diff = parseFloat(a) - parseFloat(b);
			if (diff) return diff;
		}
		if (a < b) return -1;
		if (a > b) return 1;
	}
	return 0;
}

function cullFiles(directory, library) {
	var trackMap = {};
	library.tracks.forEach(function (track) {
		var resolved = path.resolve(directory, track.path);
		trackMap[resolved] = true;
	});
	function walkDir(targetDir) {
		updateLine("Walking: " + targetDir);
		var canDelete = true;
		return Promise.wrap(fs, fs.readdir)(targetDir).then(function (entries) {
			entries.sort(magicSort);
			return Promise.map(entries, function (entry) {
				if (entry[0] === '.' || entry === 'index.html' || entry === 'media-library.js') {
					canDelete = false;
					return;
				}
				var fullEntry = path.resolve(targetDir, entry);
				return Promise.wrap(fs, fs.stat)(fullEntry).then(function (stats) {
					if (stats.isDirectory()) return walkDir(fullEntry);
					return cullFile(fullEntry);
				}).then(function (empty) {
					if (!empty) canDelete = false;
				});
			}, 1).then(function () {
				if (canDelete) {
					return Promise.wrap(fs, fs.remove)(targetDir).then(function () {
						return true;
					});
				}
				return false;
			});
		});
	}
	
	var deleted = [];
	var remaining = [];
	
	function cullFile(mediaFile) {
		if (trackMap[mediaFile]) {
			remaining.push(mediaFile);
		} else {
			return Promise.wrap(fs, fs.unlink)(mediaFile).then(function () {
				deleted.push(mediaFile);
				return true;
			});
		}
	}

	return walkDir(directory).then(function () {
		updateLine("", true);
		return {deleted: deleted, remaining: remaining};
	});
}

if (!inputDir) {
	console.error('Usage: node cull.js <media-dir>');
	process.exit(1);
}

var libraryCode = fs.readFileSync(path.join(inputDir, 'media-library.js'));
var libraryFunc = new Function('MediaLibrary', libraryCode + '\nreturn MediaLibrary;');
var library = libraryFunc();

cullFiles(inputDir, library).then(function (result) {
	console.log(result.deleted.length + ' files deleted, ' + result.remaining.length + ' files kept');
}).catch(function (error) {
	console.error(error.trace || error.stack || error);
	process.exit(error);
});
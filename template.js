var path = require('path');
var transliterate = require('transliteration').transliterate;

module.exports = function newPath(mediaFile, tags, newTemplate) {
	if (!/{/.test(newTemplate)) {
		newTemplate = path.posix.join(newTemplate || '', '{album_artist,Various Artists}/{album}/{disc,1}-{track,0} {artist} - {title}');
	}

	var lowerTags = {};
	for (var key in tags) {
		lowerTags[key.toLowerCase()] = tags[key];
	}
	if (!lowerTags.title) lowerTags.title = path.basename(mediaFile);
	if (lowerTags.track) lowerTags.track = parseInt(lowerTags.track, 10) + "";
	if (lowerTags.disc) lowerTags.disc = parseInt(lowerTags.disc, 10) + "";

	var newPath = newTemplate.replace(/\{([^}]*)\}/g, function (match, varName) {
		var result = '', defaultValue = '';
		varName.split(',').forEach(function (key) {
			var value = (lowerTags[key] || '') + "";
			var parts = key.split(':');
			if (parts.length > 1) {
				key = parts[0];
				var length = parseFloat(parts[1]) || 0;
				if (key in lowerTags) {
					value = (lowerTags[key] || "") + "";
					while (value.length < length) {
						value = '0' + value;
					}
				}
			}
			result = result || value;
			defaultValue = key;
		});
		if (result.length > 64) {
			result = result.substring(0, 40);// + sha256(result).substring(0, 8);
		}
		return transliterate(result || defaultValue).replace(/["]/g, '').replace(/[?:*,](\s)/g, '$1').replace(/[^a-zA-Z0-9\ \-\+&!()\.']/g, '_').replace(/\.*$/g, '');
	});
	var extensions = mediaFile.match(/(\.[a-z0-9]+)$/ig) || [''];
	newPath += extensions[0];
	return newPath;
};
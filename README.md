# web-media-player

This generates a very basic web-based media player as an HTML file in the media directory.

The media library is parsed from a JS file in the same directory - it also supports DropBox, so if you give it an API key you can stream from a DropBox media library.

It relies on FFmpeg for transcoding.
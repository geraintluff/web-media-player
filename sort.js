"use strict"
var fs = require('fs-extra'), path = require('path'), Promise = require('prms');

Promise.wrap = function (obj, method) {
	return function () {
		var args = [].slice.call(arguments, 0);
		return Promise.callback(function (callback) {
			method.apply(obj, args.concat([callback]));
		});
	};
};
Promise.map = function (entries, mapFn, limit) {
	entries = [].concat(entries);
	limit = limit || Infinity;
	var results = [];
	var indexCounter = 0;
	function next() {
		var index = indexCounter++;
		if (!entries.length) return;
		return Promise.resolve(mapFn(entries.shift(), index)).then(function (r) {
			results[index] = r;
			return next();
		});
	}
	var parallel = [];
	while (parallel.length < limit) {
		parallel.push(next());
	}
	return Promise.all(parallel).then(function () {
		return results;
	});
};

var inputDir = process.argv[2];
var targetTemplate = process.argv[3] || inputDir;
var fileAction = process.argv[4] || 'move';

var updateLineTimeout, updateLineTimeoutMs = 30, nextUpdateText;
function updateLine(text, sync) {
	nextUpdateText = text;
	function actualUpdate() {
		updateLineTimeout = null;
		var readline = require('readline');
		readline.clearLine(process.stdout);
		readline.cursorTo(process.stdout, 0);
		var maxLength = process.stdout.columns - 1;
		if (nextUpdateText.length > maxLength) {
			nextUpdateText = nextUpdateText.substring(0, maxLength - 3) + '...';
		}
		process.stdout.write('\r' + nextUpdateText);
	}
	if (sync) {
		actualUpdate();
	} else {
		updateLineTimeout = updateLineTimeout || setTimeout(actualUpdate, updateLineTimeoutMs);
	}
}

function magicSort(a, b, splits) {
	splits = splits || [/[^/]+/g, /[^\.]+/g, /[0-9]+|[^\s0-9]+/g];
	var number = /^[0-9]+$/;
	if (splits.length) {
		var split = splits[0];
		var subSplits = splits.slice(1);
		var aParts = a.match(split) || [], bParts = b.match(split) || [];
		while (aParts.length) {
			if (!bParts.length) return 1;
			var cmp = magicSort(aParts.shift(), bParts.shift(), subSplits);
			if (cmp) return cmp;
		}
		if (bParts.length) return -1;
	} else {
		if (number.test(a) && number.test(b)) {
			var diff = parseFloat(a) - parseFloat(b);
			if (diff) return diff;
		}
		if (a < b) return -1;
		if (a > b) return 1;
	}
	return 0;
}

function sha256(value) {
	var hash = require('crypto').createHash('sha256');
	hash.update(value);
	return hash.digest('base64').replace(/\//g, '_').replace(/\+/g, '-').replace(/=*$/, '');
}

function copyFiles(directory, newTemplate) {
	function walkDir(targetDir) {
		var canDelete = true;
		return Promise.wrap(fs, fs.readdir)(targetDir).then(function (entries) {
			entries.sort(magicSort);
			return Promise.map(entries, function (entry) {
				if (entry[0] === '.') {
					canDelete = false;
					return;
				}
				var fullEntry = path.join(targetDir, entry);
				return Promise.wrap(fs, fs.stat)(fullEntry).then(function (stats) {
					if (stats.isDirectory()) return walkDir(fullEntry);
					return copyFile(fullEntry);
				}).then(function (empty) {
					if (!empty) canDelete = false;
				});
			}, 1).then(function () {
				if (canDelete) {
					return Promise.wrap(fs, fs.remove)(targetDir).then(function () {
						return true;
					});
				}
				return false;
			});
		});
	}
	
	var skipped = {};
	var moves = 0;
	var duplicates = {};
	function copyFile(mediaFile) {
		updateLine('inspecting: ' + mediaFile);
		var ffmpeg = require('fluent-ffmpeg')(mediaFile);
		return Promise.wrap(ffmpeg, ffmpeg.ffprobe)(0).then(function (video) {
			if (!video.streams.some(function (stream) {
				return stream.codec_type === 'audio';
			})) {
				throw new Error('No audio stream');
			}
			var tags = video.format.tags || {};
			var newPath = require('./template')(mediaFile, tags, newTemplate);
			
			if (path.resolve(mediaFile) === path.resolve(newPath)) return;
			
			return Promise.wrap(fs, fs.ensureDir)(path.dirname(newPath)).then(function () {
				return new Promise(function (pass, fail) {
					fs.exists(newPath, pass);
				});
			}).then(function (exists) {
				if (exists && fileAction != 'clobber') {
					duplicates[newPath] = (duplicates[newPath] || 0) + 1;
					return;
				}
				moves++;
				var action = Promise.wrap(fs, (fileAction === 'move' || fileAction === 'clobber') ? fs.move : fs.copy);
				return action(mediaFile, newPath, {clobber: fileAction === 'clobber'});
			});
		}).catch(function (error) {
			skipped[mediaFile] = ((error.message || error) + "").split(/\r?\n/)[0];
		});
	}

	return walkDir(directory).then(function () {
		updateLine(moves + ' files copied', true);
		console.log('');
		return {moved: moves, skipped: skipped, duplicates: duplicates};
	});
}

if (!inputDir || !targetTemplate) {
	console.error('Usage: node sort.js <input-dir> /template/{album_artist}/{album}/{track}.\ {title}');
	process.exit(1);
}

copyFiles(inputDir, targetTemplate).then(function (result) {
	if (Object.keys(result.skipped).length) {
		console.log('Skipped:');
		for (var key in result.skipped) {
			console.log('\t' + key + ':\n\t\t' + result.skipped[key]);
		}
	}
	if (Object.keys(result.duplicates).length) {
		console.log('Duplicates:');
		for (var key in result.duplicates) {
			console.log('\t' + key + ': ' + result.duplicates[key]);
		}
	}
}).catch(function (error) {
	console.error(error.trace || error.stack || error);
	process.exit(error);
});
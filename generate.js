"use strict"
var fs = require('fs-extra'), path = require('path'), Promise = require('prms');
Promise.wrap = function (obj, method) {
	return function () {
		var args = [].slice.call(arguments, 0);
		return Promise.callback(function (callback) {
			method.apply(obj, args.concat([callback]));
		});
	};
};
Promise.map = function (entries, mapFn, limit) {
	entries = [].concat(entries);
	limit = limit || Infinity;
	var results = [];
	var indexCounter = 0;
	function next() {
		var index = indexCounter++;
		if (!entries.length) return;
		return Promise.resolve(mapFn(entries.shift(), index)).then(function (r) {
			results[index] = r;
			return next();
		});
	}
	var parallel = [];
	while (parallel.length < limit) {
		parallel.push(next());
	}
	return Promise.all(parallel).then(function () {
		return results;
	});
};

var inputDir = process.argv[2];
var targetDir = process.argv[3] || inputDir;
var fileTypes = process.argv[4] || 'mp3,m4a';
var newTemplate = process.argv[5];

var updateLineTimeout, updateLineTimeoutMs = 30, nextUpdateText;
function updateLine(text, sync) {
	nextUpdateText = text;
	function actualUpdate() {
		updateLineTimeout = null;
		var readline = require('readline');
		readline.clearLine(process.stdout);
		readline.cursorTo(process.stdout, 0);
		var maxLength = process.stdout.columns - 1;
		if (nextUpdateText.length > maxLength) {
			nextUpdateText = nextUpdateText.substring(0, maxLength - 3) + '...';
		}
		process.stdout.write('\r' + nextUpdateText);
	}
	if (sync) {
		actualUpdate();
	} else {
		updateLineTimeout = updateLineTimeout || setTimeout(actualUpdate, updateLineTimeoutMs);
	}
}

function magicSort(a, b, splits) {
	splits = splits || [/[^/]+/g, /[^\.]+/g, /[0-9]+|[^\s0-9]+/g];
	var number = /^[0-9]+$/;
	if (splits.length) {
		var split = splits[0];
		var subSplits = splits.slice(1);
		var aParts = a.match(split), bParts = b.match(split);
		if (!aParts || !bParts) {
			console.log([a, b, split, subSplits]);
			throw new Error('no match');
		}
		while (aParts.length) {
			if (!bParts.length) return 1;
			try {
				var cmp = magicSort(aParts.shift(), bParts.shift(), subSplits);
			} catch (e) {
				console.log([a, b, split, subSplits]);
				throw e;
			}
			if (cmp) return cmp;
		}
		if (bParts.length) return -1;
	} else {
		if (number.test(a) && number.test(b)) {
			var diff = parseFloat(a) - parseFloat(b);
			if (diff) return diff;
		}
		if (a < b) return -1;
		if (a > b) return 1;
	}
	return 0;
}

var SCAN_PARALLEL = 4, ENCODE_PARALLEL = 4;

function createCatalog(directory, transcodeDirectory, fileMatch) {
	directory = path.resolve(directory);
	transcodeDirectory = path.resolve(transcodeDirectory);

	/*
	var hashes = {};
	function hash(file) {
		return hashes[file] = hashes[file] || new Promise(function (pass, fail) {
			var hash = require('crypto').createHash('sha256');
			var ffmpeg = require('fluent-ffmpeg')(file);
			console.log('\nhashing '+ file + '\n');
			var stream = ffmpeg.noVideo().audioCodec('copy').format('wav').pipe();
			stream.on('error', fail);
			stream.on('data', function (buffer) {
				hash.update(buffer);
			});
			stream.on('end', function () {
				pass(hash.digest('hex'));
			});
		});
	}
	*/
	
	var signatures = {};
	function findDuplicate(entry, file) {
		var tags = entry.tags;
		var signature = [];
		if (!tags.title || !tags.artist) return Promise.resolve(false);
		['album', 'artist', 'album_artist', 'title', 'track', 'disc', 'version'].forEach(function (key) {
			signature.push(tags[key]);
		});
		signature = JSON.stringify(signature);
		if (signature in signatures) {
			return Promise.resolve(signatures[signature].path);
			/*
			var files = signatures[signature] = [file].concat(signatures[signature]);
			return Promise.all(files.map(hash)).then(function (hashes) {
				for (var i = 1; i < hashes.length; i++) {
					console.log('hashes: ' + hashes[0] + ' - ' + hashes[i]);
					if (hashes[i] === hashes[0]) {
						return true;
					}
				}
				return false;
			});
			*/
		}
		signatures[signature] = entry;
		return Promise.resolve(false);
	}
	
	var catalog = {
		tracks: [],
		skipped: {},
		errors: {}
	};
	var regexEscape = RegExp.escape || function(text) {
		return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
	};
	fileMatch = fileMatch || /\.(mp3|m4a)$/i;
	if (typeof fileMatch === 'string') {
		fileMatch = new RegExp('\\.(' + fileMatch.split(',').map(regexEscape).join('|') + ')$', 'i');
	}
	var files = [];
	
	function addDir(targetDir, pathPrefix) {
		updateLine('walking: ' + path.relative(directory, targetDir));
		// Avoid walking the transcode directory
		if (directory !== transcodeDirectory && targetDir === transcodeDirectory) return;
		
		return Promise.wrap(fs, fs.readdir)(targetDir).then(function (entries) {
			entries.sort(magicSort);
			return Promise.map(entries, function (entry) {
				if (entry[0] === '.') return;
				var fullEntry = path.join(targetDir, entry);
				var fullPath = path.posix.join(pathPrefix, entry);
				return Promise.wrap(fs, fs.stat)(fullEntry).then(function (stats) {
					if (stats.isDirectory()) return addDir(fullEntry, fullPath);
					files.push({file: fullEntry, path: fullPath, stats: stats});
				});
			}, 1);
		});
	}
	var startMs = Date.now();
	return addDir(directory, '').then(function () {
		var transcodes = [];
		return Promise.map(files, function (entry) {
			var fullEntry = entry.file, fullPath = entry.path, stats = entry.stats;
			updateLine('scanning: ' + path.relative(directory, fullEntry));
			var ffmpeg = require('fluent-ffmpeg')(fullEntry);

			var catalogEntry = {
				path: fullPath,
				tags: {},
				size: stats.size
			};
			return Promise.wrap(ffmpeg, ffmpeg.ffprobe)(0).then(function (video) {
				if (!video.streams.some(function (stream) {
					return stream.codec_type === 'audio';
				})) {
					throw new Error('No audio stream');
				}
				{};
				var tags = video.format.tags || {};
				for (var key in tags) {
					catalogEntry.tags[key.toLowerCase()] = tags[key];
				}

				return findDuplicate(catalogEntry, fullEntry).then(function (duplicate) {
					if (duplicate) throw new Error('Duplicate track: ' + duplicate);
					catalog.tracks.push(catalogEntry);
					return video;
				});
			}).catch(function (error) {
				var relativePath = path.relative(directory, fullEntry);
				catalog.skipped[relativePath] = error.message;
			}).then(function (video) {
				if (!video) return;
				if (newTemplate) {
					catalogEntry.path = require('./template')(catalogEntry.path, catalogEntry.tags, newTemplate);
				}
				if (directory !== transcodeDirectory) {
					var isAllowedType = fileMatch.test(fullEntry);
					if (!isAllowedType) {
						catalogEntry.path += '.mp3';
					}
					if (newTemplate) {
						catalogEntry.path = require('./template')(catalogEntry.path, catalogEntry.tags, newTemplate);
					}
					var targetFile = path.join(transcodeDirectory, catalogEntry.path);
					return Promise.wrap(fs, fs.ensureDir)(path.dirname(targetFile)).then(function () {
						return new Promise(function (pass, fail) {
							fs.exists(targetFile, pass);
						});
					}).then(function (exists) {
						if (!exists) return false;

						// Check durations match before re-using it
						var ffmpeg = require('fluent-ffmpeg')(targetFile);
						return Promise.wrap(ffmpeg, ffmpeg.ffprobe)(0).then(function (existingVideo) {
							var diff = existingVideo.format.duration - video.format.duration;
							return Math.abs(diff) < 0.1;
						}).catch(function (e) {
							return false;
						});
					}).then(function (exists) {
						if (exists) return;
						if (isAllowedType) {
							return Promise.wrap(fs, fs.copy)(fullEntry, targetFile);
						}
						transcodes.push({source: fullEntry, target: targetFile, path: path.relative(transcodeDirectory, targetFile)});
					});
				}
			});
		}, SCAN_PARALLEL).then(function () {
			return transcodes;
		});
	}).then(function (transcodes) {
		return Promise.map(transcodes, function(transcode, index) {
			return new Promise(function (pass, fail) {
				updateLine('encoding ' + (index + 1) + '/' + transcodes.length + ': ' + transcode.path);
				var ffmpeg = require('fluent-ffmpeg');
				var transcoder = ffmpeg(transcode.source).audioCodec('libmp3lame').audioBitrate(256).output(transcode.target);
				transcoder.on('end', pass);
				transcoder.on('error', fail);
				transcoder.run();
			}).catch(function (error) {
				catalog.errors[transcode.path] = error.message;
				/*
				return Promise.wrap(fs, fs.unlink)(transcode.target).catch(function (e) {
					if (e.code !== 'ENOENT') throw e;
				});
				*/
			});
		}, ENCODE_PARALLEL);
	}).then(function () {
		updateLine('', true);
		var endMs = Date.now();
		console.log(catalog.tracks.length + ' tracks');
		console.log('\t' + (endMs - startMs)/1000 + 's');
		if (Object.keys(catalog.skipped).length) {
			console.log('\tskipped:');
			for (var path in catalog.skipped) {
				console.log('\t\t' + path + ':\n\t\t\t' + catalog.skipped[path].split(/\r?\n/g)[0]);
			}
		}
		if (Object.keys(catalog.errors).length) {
			console.log('\terrors:');
			for (var path in catalog.errors) {
				console.log('\t\t' + path + ':\n\t\t\t' + catalog.errors[path].split(/\r?\n/g)[0]);
			}
		}
		
		catalog.tracks.sort(function (a, b) {
			if (!a.path || !b.path) {
				console.log(a, b);
				process.exit();
			}
			return magicSort(a.path, b.path);
		});
		return catalog;
	});
}

if (!inputDir) {
	console.error('Usage: generate-web-media <input-dir> <?mp3-dir> <?extensions=mp3,mp4>');
	process.exit(1);
}

createCatalog(inputDir, targetDir, fileTypes).then(function (catalog) {
	var libraryFile = path.join(targetDir, 'media-library.js');
	var jsCode = 'MediaLibrary = ' + JSON.stringify(catalog, null, '\t') + ';';
	jsCode = jsCode.replace(/[\u007f-\uffff]/g, function(unicodeChar) {
		var charCode = unicodeChar.charCodeAt(0);
		return '\\u' + ('000' + charCode.toString(16)).slice(-4);
	});
	return Promise.wrap(fs, fs.outputFile)(libraryFile, jsCode).then(function () {
		return Promise.wrap(fs, fs.copy)(__dirname + '/index.html', path.join(targetDir, 'index.html'));
	});
}).catch(function (error) {
	console.error(error.trace || error.stack || error);
	process.exit(error);
});